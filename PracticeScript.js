var myApp = angular.module("myModule", []).controller(
  "myController", function($scope) {
    var countries = [
      {
        name: "UK",
        cities: [
          {name: "London"},
          {name: "Manchester" },
          {name: "Birmingham"}
        ]
      },
      {
        name: "USA",
        cities: [
        {name: "Los Angeles"},
        {name: "New York" },
        {name: "Washington D.C"}
      ]
    }
  ];

  $scope.countries = countries;
});
